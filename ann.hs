{-# LANGUAGE OverloadedStrings #-}
module Annuity where

import           Language.Marlowe
import           Ledger.Crypto
import           Data.ByteString.Lazy.Internal

main :: IO ()
main = print . pretty $ contract

loanee :: PubKey
loanee = "alice"

loaner :: PubKey
loaner = "bob"

notionalPrincipal :: (Num t) => t
notionalPrincipal = 10000

nominalInterestRate :: (Num t) => t
nominalInterestRate = 5

interestCycle :: (Num t) => t
interestCycle = 10

installmentCount :: (Num t) => t
installmentCount = 10

payoffStartSlot :: (Num t) => t
payoffStartSlot = 20

contract :: Contract
contract =
    (When
        [Case (Deposit (AccountId 0 loaner) loaner (Constant notionalPrincipal))
          (Pay (AccountId 0 loaner) (Party loanee) (Constant notionalPrincipal)
            (scheduleInterestPayment (calculateInstallmentValue) installmentCount payoffStartSlot)
          )
        ]
    10
    Close
    )

getChoiceValue :: ByteString -> PubKey -> Value
getChoiceValue choiceName party =
  ChoiceValue (ChoiceId choiceName party) (Constant 0)

scheduleInterestPayment :: Integer -> Integer -> Integer -> Contract
scheduleInterestPayment _ 0 _ =
  Close

scheduleInterestPayment payment installmentsLeft timeout =
  When [Case (Deposit (AccountId 0 loanee) loanee (Constant payment))
    (Pay (AccountId 0 loanee) (Party loaner) (Constant payment)
      (scheduleInterestPayment payment (installmentsLeft - 1) (timeout + interestCycle))
    )
  ]
  (Slot timeout)
  Close

calculateInstallmentValue :: Integer
calculateInstallmentValue =
  let c = notionalPrincipal
      r = nominalInterestRate / 100
      n = installmentCount
      rn = r / 12
  in
      ceiling ((c * rn) / (1 - ((1 + rn) ** (- n))))
